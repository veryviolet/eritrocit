#!/usr/bin/env python

import sys
import os
import numpy as np
import datetime
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import click
import cv2
import matplotlib.pyplot as plt

import eritrocit
from eritrocit.video import VideoProcessor
from eritrocit.bloodflow import get_flows

WINDOW_SIZE = 30
TOTAL_FRAMES = 100

@click.command()
def fmain():

    click.secho('\n\n Eritrocit v{} \n\n'.format(eritrocit.__version__))

    click.secho('  *** camera processing *** \n')

    video_batcher = VideoProcessor(window=1000)

    cap = cv2.VideoCapture(0)

    fps = cap.get(cv2.CAP_PROP_FPS)

    started = False
    stopped = False
    start_time = None
    finish_time = None

    count = 0

    while (True):
        # Capture frame-by-frame
        ret, frame = cap.read()

 #       batch = video_batcher.get_buffer()

#        if batch is not None:
#            blood = get_flow_map(batch, fps, 10, 10)
#            cv2.imshow('bloodflow', blood)

        #            mn = np.mean(batch, axis=0).astype(dtype=np.uint8)

#            avbatch = np.squeeze(mn)

        center = (int(frame.shape[0]/2), int(frame.shape[1]/2))

        if started:
           video_batcher.process_frame(frame[center[0]-WINDOW_SIZE:center[0]+WINDOW_SIZE,
                                       center[1]-WINDOW_SIZE:center[1]+WINDOW_SIZE, :])
           count += 1

        cv2.rectangle(frame, (center[1]-WINDOW_SIZE, center[0]-WINDOW_SIZE),
                      (center[1]+WINDOW_SIZE, center[0]+WINDOW_SIZE),
                      (0, 0, 255) if started else (0, 255, 0),
                      4)

        # Display the resulting frame
        cv2.imshow('source', frame)

        key = cv2.waitKey(1)

        if key & 0xFF == ord('q'):
            break
        elif key & 0xFF == ord('s'):
            if not started:
                started = True
                start_time = datetime.datetime.now()
                count += 1
                stopped = False

        if count == TOTAL_FRAMES:
            count = 0
            started = False
            finish_time = datetime.datetime.now()
            stopped = True
            buf = video_batcher.get_buffer()
            real_fps = int(buf.shape[0] / (finish_time - start_time).seconds)
            get_flows(buf, real_fps, pathtosave='./', show=True)
            video_batcher.clear_buffer()


    # When everything done, release the capture

    cap.release()
    cv2.destroyAllWindows()

    click.secho('\n\n')


if __name__ == '__main__':
    fmain()
