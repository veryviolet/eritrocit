import numpy as np

def get_flow_map(frames: np.ndarray, fps: int, blocks_y: int, blocks_x: int) -> np.ndarray:

    heatmap = np.zeros((blocks_y, blocks_x))

    step_x = frames.shape[2] // blocks_x
    step_y = frames.shape[1] // blocks_y

    for y in range(blocks_y):
        for x in range(blocks_x):
            signal = get_blood_flow_signal(frames[:, (y*step_y):((y+1)*step_y), (x*step_x):((x+1)*step_x), :], fps=fps)
            heatmap[y, x] = get_blood_flow_presence_score(signal, fps)

    heatmap = heatmap/np.max(heatmap, axis=(0,1))

    return heatmap