import numpy as np
import numpy.fft as fft
import scipy.signal as signal
from scipy.stats import entropy
import matplotlib.pyplot as plt
import os

SKIN_TONE_STD_RGB = [0.7682, 0.5121, 0.3841]
LOWEST_BPM = 40
HIGHEST_BPM = 240
LOWEST_BPS = LOWEST_BPM / 60
HIGHEST_BPS = HIGHEST_BPM / 60

LowHB = 40 / 60
HighHB = 100 / 60


def moving_average(x, window, padding='same'):
    ret = np.zeros_like(x)

    for c in range(x.shape[1]):
        ret[:, c] = np.convolve(x[:, c], np.ones(window), padding) / window

    return ret

def get_freqs_by_signal(signal: np.ndarray, fps: int, cut: bool = True):
    LowHB = 40 / 60
    HighHB = 100 / 60
    S = signal.shape[-1]
    T = S / fps
    dT = 1 / fps
    spectrum = fft.fft(signal)
    freqs = fft.fftfreq(S, d=dT)
    if cut:
        spectrum[np.abs(freqs) > HighHB] = 0
        spectrum[np.abs(freqs) < LowHB] = 0
#    plt.plot(freq[0:S // 2], abs(spectrum)[0:S // 2])
    return freqs[0:S // 2], abs(spectrum)[0:S // 2]

def get_blood_flow_presence_score(spectrum: np.ndarray):
    normedspectrum = spectrum/np.max(spectrum)
    entr = entropy(normedspectrum)
    maxentr = np.log2(normedspectrum.shape[0])
#    return np.max(np.abs(cut_fspace))
 #   cut_sample = irfft(cut_fspace)
    return maxentr - entr if entr is not None and not np.isnan(entr) else 0


def get_blood_flow_signal(video, fps, bandpass=False):
    """
    :param video: axis are (time, height, width, 3)
    :return: blood flow signal; shape=(t,)
    """
#    rgb_signal = []
#    for frame in video:
#        assert isinstance(frame, np.ndarray)
#        assert len(frame.shape) == 2
#        assert frame.shape[1] == 3
#        assert frame.dtype == np.uint8
#        RGB = np.mean(frame, axis=(0, 1))
#    rgb_signal = np.array(rgb_signal)

    rgb_signal = np.mean(video, axis=(1, 2))

    # make C_n which is C/avg_window(C) (avg by time)
    # calculate X_s, Y_s and their std
    # calculate alpha
    # C_f (bandpass C_n)
    # S

    movavg_window_frames_len = int(round(fps / LOWEST_BPS))
    rgb_n = moving_average(rgb_signal, movavg_window_frames_len)
    rgb_n = np.transpose(rgb_n)  # now (RGB, time)

    rgb_f = rgb_n
    x_f = 3 * rgb_n[0] - 2 * rgb_n[1]
    y_f = 1.5 * rgb_n[0] + rgb_n[1] - 1.5 * rgb_n[2]

    if bandpass:
        n_f = 5
        f = signal.firwin(n_f, cutoff=[LOWEST_BPS, HIGHEST_BPS], fs=fps)
        rgb_f = np.array([np.convolve(C_signal, f, mode='same') for C_signal in rgb_n])
        x_f = np.convolve(x_f, f, mode='same')
        y_f = np.convolve(y_f, f, mode='same')


    std_X_f = np.std(x_f)
    std_Y_f = np.std(y_f)
    alpha = std_X_f / std_Y_f
    alpha_32 = 1.5 * alpha
    S = (3 - alpha_32) * rgb_f[0] - (2 + alpha) * rgb_f[1] + alpha_32 * rgb_f[2]

    make_even = True
    if make_even:
        if len(S) % 2 == 1:
            S = S[:len(S) - 1]

    return S

def plot_one_graph(data, title, filename, show):
    f = plt.figure(figsize=(20, 10))
    f.suptitle(title)
    if len(data.shape) == 1:
        plt.plot(data)
    else:
        plt.plot(data[:,0], data[:, 1])
    plt.savefig(filename)
    plt.show()



def get_flows(buffer: np.ndarray, fps: int, pathtosave: str, show: bool):
    rgb_mean = np.mean(buffer, axis=(1, 2))
    movavg_window_frames_len = int(round(fps / LOWEST_BPS))
    rgb_n = moving_average(rgb_mean, movavg_window_frames_len)

    BLUE = 0
    GREEN = 1
    RED = 2

    RoverG = rgb_n[:, RED] / rgb_n[:, GREEN] - 1

    RoverG = RoverG[movavg_window_frames_len:]

    RoverG_freqs, RoverG_spectre = get_freqs_by_signal(RoverG, fps)

    RoverG_spectre = np.vstack((RoverG_freqs, RoverG_spectre)).T

    np.save(os.path.join(pathtosave, 'roverg_raw.npy'), RoverG)
    plot_one_graph(RoverG, "RoverG raw", os.path.join(pathtosave, 'roverg_raw.png'), True)
    plot_one_graph(RoverG_spectre, "RoverG spectre", os.path.join(pathtosave, 'roverg_spectre.png'), True)

    X = rgb_n[:, RED] - rgb_n[:, GREEN]
    Y = 0.5*rgb_n[:, RED] + 0.5*rgb_n[:, GREEN] - rgb_n[:, BLUE]

    XoverY = X/Y - 1

    XoverY = XoverY[movavg_window_frames_len:]

    XoverY_freqs, XoverY_spectre = get_freqs_by_signal(XoverY, fps)

    XoverY_spectre = np.vstack((XoverY_freqs, XoverY_spectre)).T

    np.save(os.path.join(pathtosave, 'xovery_raw.npy'), XoverY, XoverY)
    plot_one_graph(XoverY, "XoverY raw", os.path.join(pathtosave, 'xovery_raw.png'), True)
    plot_one_graph(XoverY_spectre, "XoverY spectre", os.path.join(pathtosave, 'xovery_spectre.png'), True)

    pass

