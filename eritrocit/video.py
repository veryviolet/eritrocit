import numpy as np
import cv2

DEFAULT_WINDOW = 5

class VideoProcessor:

    _buffer = None

    _window = None
    _downsample = None

    def __init__(self, filename=None, window=DEFAULT_WINDOW, downsample=1.0):

        self._window = window
        self._downsample = downsample

    def clear_buffer(self):
        del self._buffer
        self._buffer = None

    def resize_frame(self, frame: np.ndarray):

        return frame

#        if self._downsample < 1.0:
#            width = int(frame.shape[1] * self._downsample)
#            height = int(frame.shape[0] * self._downsample)
#            dim = (width, height)
#            # resize image
#            resized = cv2.resize(frame, dim, interpolation=cv2.INTER_AREA)
#            return resized
##            return frame

    def process_frame(self, frame: np.ndarray):

        resized = self.resize_frame(frame)
        resized = np.expand_dims(resized, 0)

        if self._buffer is None:
            self._buffer = resized
        else:
            self._buffer = np.concatenate((self._buffer, resized), axis=0)

            if self._buffer.shape[0] > self._window:
                self._buffer = self._buffer[1:, :, :, :]

    def get_buffer(self) -> np.ndarray:
        return self._buffer #if self._buffer.shape[0] == self._window else None


